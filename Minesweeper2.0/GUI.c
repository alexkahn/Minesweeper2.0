#include "GUI.h"

/*
 *
 * Op het internet kan je verschillende tutorials voor SDL vinden.
 * Zie bv. http://www.parallelrealities.co.uk/2011/09/basic-game-tutorial-1-opening-window.html
 * Of http://lazyfoo.net/SDL_tutorials/
 * Of https://wiki.libsdl.org/FrontPage (bevat documentatie voor de verschillende functies
 * en types die gebruikt worden in SDL)
 */

/*
 * Dit is het venster dat getoond zal worden en waarin het speelveld weergegeven wordt.
 * Dit venster wordt aangemaakt bij het initialiseren van de GUI en wordt weer afgebroken
 * wanneer het spel eindigt.
 */
static SDL_Surface *window;

/*
 * Deze array bevat de 12 afbeeldingen die gebruikt worden tijdens het spel:
 * images[0]: de afbeelding die een onthuld vak met 0 mijnen als buur voorstelt
 * images[1]: de afbeelding die een onthuld vak met 1 mijn als buur voorstelt
 * images[2]: de afbeelding die een onthuld vak met 2 mijnen als buur voorstelt
 * images[3]: de afbeelding die een onthuld vak met 3 mijnen als buur voorstelt
 * images[4]: de afbeelding die een onthuld vak met 4 mijnen als buur voorstelt
 * images[5]: de afbeelding die een onthuld vak met 5 mijnen als buur voorstelt
 * images[6]: de afbeelding die een onthuld vak met 6 mijnen als buur voorstelt
 * images[7]: de afbeelding die een onthuld vak met 7 mijnen als buur voorstelt
 * images[8]: de afbeelding die een onthuld vak met 8 mijnen als buur voorstelt
 * images[9]: de afbeelding die een vak met een vlag voorstelt
 * images[10]: de afbeelding die een niet-onthuld vak voorstelt
 * images[11]: de afbeelding die een vak met een mijn voorstelt
 */
SDL_Surface* images[12];

/*
 * Maakt het hele venster blanco: deze functie kan je eventueel aanroepen voor
 * je iets begint te tekenen in het venster, zodat alle afbeeldingen die eerder
 * in het venster getekend waren verwijderd worden.
 * Aan deze functie hoef je normaal niets aan te passen.
 */
void clear_screen() {
    SDL_FillRect(window, NULL, 0xFFFFFFFF);
    SDL_Flip(window);
}

/*
 * Deze functie moet je zelf vervolledigen: je mag alles aan deze functie
 * (inclusief return-type en argumenten) aanpassen, indien nodig.
 */
void draw_cell(int x, int y){
    SDL_Rect offset;
    offset.x = x*IMAGE_WIDTH;
    offset.y = y*IMAGE_HEIGHT;
    
    if ((get_cell(x, y)->state) == COVERED) {
        SDL_BlitSurface(images[10], NULL, window, &offset);
    }
    else if ((get_cell(x, y)->state) == FLAGGED){
        SDL_BlitSurface(images[9], NULL, window, &offset);
    }
    else if ((get_cell(x, y)->state) == UNCOVERED){
        if (get_cell(x, y)->is_mine == 0) {
            SDL_BlitSurface(images[get_cell(x, y)->neighbouring_mines], NULL, window, &offset);
        }
        else{
            SDL_BlitSurface(images[11], NULL, window, &offset);
        }
    }
    
}
void draw_grid(int width, int height, int nr_of_mines) {
    
    /*
     * Onderstaande code moet zeker worden uitgevoerd op het einde van deze functie.
     * Wanneer je iets tekent in het venster wordt dit venster nl. niet meteen aangepast.
     * Via de SDL_Flip functie wordt het venster correct geupdatet.
     */
    for (int y = 0; y < height; y++){
        for (int x = 0; x < width; x++){
            draw_cell(x, y);
        }
    }
    //run_game(height, width, nr_of_mines);
    SDL_Flip(window);
    
}
void uncover_cell(int x, int y){
    SDL_Rect offset;
    offset.x = x*IMAGE_WIDTH;
    offset.y = y*IMAGE_HEIGHT;
    if ((get_cell(x, y)->state) == FLAGGED){
        SDL_BlitSurface(images[9], NULL, window, &offset);
    }
    else {
        if (get_cell(x, y)->is_mine == 0) {
            SDL_BlitSurface(images[get_cell(x, y)->neighbouring_mines], NULL, window, &offset);
        }
        else{
            SDL_BlitSurface(images[11], NULL, window, &offset);
        }
        
        
    }
}
void uncover_grid(int width, int height){
    for (int y = 0; y < height; y++){
        for (int x = 0; x < width; x++){
            uncover_cell(x, y);
        }
    }
    SDL_Flip(window);
}


/*
 * Vangt de input uit de GUI op. Deze functie is al deels geïmplementeerd, maar je moet die zelf
 * nog afwerken. Je mag opnieuw alles aanpassen aan deze functie, inclusies return-type en argumenten.
 */
void read_input(int grid_height, int grid_width, int nr_of_mines) {
    
    SDL_Event event;
    
    int mouse_x;
    int mouse_y;
    
    /*
     * Handelt alle input uit de GUI af.
     * Telkens de speler een input aan de GUI geeft (bv. een muisklik, muis bewegen, toets indrukken enz.)
     * wordt er een 'event' (van het type SDL_Event) gegenereerd dat hier wordt afgehandeld.
     *
     * Let wel op: niet al deze events zijn relevant voor jou: als de muis bv. gewoon wordt bewogen, hoef
     * je niet te reageren op dit event. Je zal dus (eventueel) een manier moeten vinden om alle niet-relevante
     * events weg te filteren.
     *
     * Zie ook https://wiki.libsdl.org/SDL_PollEvent en http://www.parallelrealities.co.uk/2011_09_01_archive.html
     */
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                // De speler wil het spel afsluiten.
                deallocate_grid(grid_width, grid_height);
                exit(1);

            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                    case SDLK_p:
                        // De speler heeft op de P toets gedrukt.
                        print_grid_uncovered(grid_width, grid_height, nr_of_mines);
                        uncover_grid(grid_width, grid_height);
                    default:
                        // De speler heeft op een andere toets gedrukt.
                        // Deze printf mag je verwijderen.
                        printf("toets ingedrukt\n");
                }
                break;
                
            case SDL_MOUSEBUTTONDOWN: //Met if moeten werken omdat er anders geen verschil was tussen links en rechts
                /*
                 * De speler heeft met de muis geklikt: met de onderstaande lijn worden de coördinaten in het
                 * het speelveld waar de speler geklikt heeft bewaard in de variabelen mouse_x en mouse_y.
                 */
                SDL_GetMouseState(&mouse_x, &mouse_y);
                
                if(event.button.button == SDL_BUTTON_RIGHT){ //Had een if-test nodig, anders was er geen verschil tussen rechts en links, nu wel door lazy evalutation
                    // De gebruiker heeft met de rechtermuisknop geklikt.
                    // Deze printf mag je verwijderen.
                    printf("muis geklikt\n");
                    flag_coordinate(mouse_x/IMAGE_WIDTH, mouse_y/IMAGE_HEIGHT, grid_height, grid_width, nr_of_mines);
                    draw_grid(grid_width, grid_height, nr_of_mines);
                }
                else if (event.button.button == SDL_BUTTON_LEFT){
                    // De gebruiker heeft met de linkermuisknop geklikt.
                    reveal_coordinate(mouse_x/IMAGE_WIDTH, mouse_y/IMAGE_HEIGHT, grid_height, grid_width, nr_of_mines);
                    draw_grid(grid_width, grid_height, nr_of_mines);
                }
        }
        
    }
    
}


/*
 * Met de code hieronder moet je niet echt rekening houden. Deze code hoef je enkel aan te passen
 * als je iets wil veranderen aan het initialiseren van de GUI.
 *
 * Let wel op, de functie initialize_gui moet aan de start van het programma zeker aangeroepen worden!
 *
 * De main-functie die helemaal onderaan gedefinieerd staat, moet je ook wel verwijderen. Deze functie
 * staat er enkel zodat je meteen kan controleren of de SDL library werkt op jouw computer, maar je
 * hebt deze functie niet nodig om je project te maken.
 */

void stop_gui(void) {
    SDL_Quit();
}

/*
 * Laadt de afbeeldingen die getoond moeten worden in. Alle afbeeldingen worden samen bijgehouden
 * in een array 'images'.
 */
void initialize_figures() {
    images[0] = SDL_LoadBMP("Images/0.bmp");
    images[1] = SDL_LoadBMP("Images/1.bmp");
    images[2] = SDL_LoadBMP("Images/2.bmp");
    images[3] = SDL_LoadBMP("Images/3.bmp");
    images[4] = SDL_LoadBMP("Images/4.bmp");
    images[5] = SDL_LoadBMP("Images/5.bmp");
    images[6] = SDL_LoadBMP("Images/6.bmp");
    images[7] = SDL_LoadBMP("Images/7.bmp");
    images[8] = SDL_LoadBMP("Images/8.bmp");
    images[9] = SDL_LoadBMP("Images/flagged.bmp");
    images[10] = SDL_LoadBMP("Images/covered.bmp");
    images[11] = SDL_LoadBMP("Images/mine.bmp");
}

/*
 * Initialiseer het venster.
 */
void initialize_window(char *title, int grid_width, int grid_height) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Could not initialize SDL: %s\n", SDL_GetError());
        exit(1);
    }
    
    int window_width = grid_width * IMAGE_WIDTH;
    int window_height = grid_height * IMAGE_HEIGHT;
    window = SDL_SetVideoMode(window_width, window_height, 0, SDL_HWPALETTE | SDL_DOUBLEBUF);
    if (window == NULL) {
        printf("Couldn't set screen mode to required dimensions: %s\n", SDL_GetError());
        exit(1);
    }
    /* Set the screen title */
    SDL_WM_SetCaption(title, NULL);
}

/*
 * Initialiseer de GUI. De functie krijgt de breedte en de hoogte van het speelveld mee als argumenten.
 */
void initialize_gui(int grid_width, int grid_height, int nr_of_mines) {
    initialize_window("Minesweeper", grid_width, grid_height);
    initialize_figures();
    SDL_Event event;
    SDL_PollEvent(&event);
    draw_grid(grid_width, grid_height, nr_of_mines);
    
    atexit(stop_gui);
}
