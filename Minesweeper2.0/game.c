#include "game.h"
#include "stdbool.h"

/****************************************************************************************************
 *								  		TE IMPLEMENTEREN FUNCTIES:						  			*
 ****************************************************************************************************/




int nr_of_remaining_flags = 0;

int correct_flags = 0;

bool continue_game = true;

int nr_of_plays = 0;

int nr_of_unveiled = 0;


// Alle stapjes die moeten gebeuren om een spel te herstarten
void restart_game(int height, int width, int nr_of_mines){
    nr_of_unveiled = 0;
    nr_of_plays = 0;
    correct_flags = 0;
    deallocate_grid(width, height);
    initialize_grid(width, height, nr_of_mines);
    nr_of_remaining_flags = nr_of_mines;
    read_input(height, width, nr_of_mines);
    draw_grid(width, height, nr_of_mines); //Zorgt dat het scherm weer gevuldt wordt met volle vakjes
}
// Kleine check om te zien of de gekozen coordinaten wel geldig zijn
bool check_coordinate(int x, int y, int height, int width){
    return (x >= 0 && x< width && y >= 0 && y < width);
}

/*
 * Deze functie wordt aangeroepen als de gebruiker een vlag wil plaatsten op het vakje met positie (x,y)
 * in het veld.
 */
void flag_coordinate(int x, int y, int height, int width, int nr_of_mines){
    if (check_coordinate(x, y, height, width)){
        if(get_cell(x, y)->state==FLAGGED){ //Als er een vlag staat wordt deze weggehaald.
            nr_of_remaining_flags++;
            get_cell(x, y)->state=COVERED;
            print_grid(nr_of_remaining_flags, height, width);
            if (get_cell(x, y)->is_mine==1){
                correct_flags--;
            }
        }
        else if (nr_of_remaining_flags > 0) { //Als er geen vlag staat moet er een geplaatst worden.
            nr_of_remaining_flags--;
            get_cell(x, y)->state=FLAGGED;
            print_grid(nr_of_remaining_flags, height, width);
            if (get_cell(x, y)->is_mine==1){
                correct_flags++;
            }
        }
        else (printf("Not enough flags"));
    }
    else printf("Outside of grid");
}

/*
 * Deze functie wordt aangeroepen als de gebruiker het vakje met positie (x,y) in het veld wil onthullen.
 */
void reveal_neighbours(int x, int y, int heigth, int width){ // Deze functie wordt aangeroepen indien de buren onthuld moeten worden.
    for (int y_co = -1; y_co < 2; y_co++){
        for (int x_co = -1; x_co < 2; x_co++){
            if (!(x_co == 0 && y_co == 0) &&  (x + x_co >= 0) && (y + y_co >= 0) && (x + x_co < width) && (y + y_co < heigth) && get_cell(x +x_co, y+y_co)->state==COVERED && get_cell(x +x_co, y+y_co)->is_mine==0){
                get_cell(x +x_co, y+y_co)->state=UNCOVERED;
                nr_of_unveiled++;
                if(get_cell(x + x_co, y + y_co)->neighbouring_mines==0){
                    reveal_neighbours(x + x_co, y + y_co, heigth, width); // Indien nodig roept deze functie zich zelf recursief op
                }
            }
        }
    }
}
void reveal_coordinate(int x, int y, int height, int width,int nr_of_mines){ // Deze functie wordt aangeroepen om een vakje te onthullen
    if (check_coordinate(x, y, height, width)){
        get_cell(x, y)->state=UNCOVERED;
        nr_of_unveiled++;
        if (get_cell(x, y)->is_mine == 1) { // Als de cel een mijn is
            if (nr_of_plays == 0){  // en dit de eerste beurt is, wordt het spel gereinitialiseerd
                initialize_grid(width, height, nr_of_mines);
                reveal_coordinate(x, y, height, width, nr_of_mines); // en dezelfde coördinaten opnieuwe getoond
                nr_of_plays++;
            }
            else{ // Anders is het spel gedaan en wordt het spel ook gereinitiealiseerd
                printf("You lost, try again \n");
                save_to_file(0);
                restart_game(height, width, nr_of_mines);
            }
        }
        else if (get_cell(x, y)->neighbouring_mines == 0) { // indien er geen mijn is en ook geen mijn als buur
            reveal_neighbours(x, y, height, width);
            nr_of_plays++;
            print_grid(nr_of_remaining_flags, height, width);
        }
        else { // in het meest simpele geval
            nr_of_plays++;
            print_grid(nr_of_remaining_flags, height, width);
        }
    }
    else printf("Outside of grid");
}

/*
 * Deze functie wordt aangeroepen om het hele, onthulde, veld te printen.
 */
void print_grid_uncovered(int height, int width, int nr_of_mines){ //geeft tijdelijk de positie van alles weer. Telt niet als beurt, eerder als methode om te spieken
    uncover(nr_of_remaining_flags, height, width);
    //handle_input(height, width, nr_of_mines);
    read_input(height, width, nr_of_mines);
}

/*
 * Deze functie wordt in het begin, eens het veld geïnitialiseerd is, aangeroepen.
 * Eens deze functie is aangeroepen moet het spel in beurten verdergaan tot de speler gewonnen of verloren heeft.
 * Na elke beurt moet het hele veld automatisch geprint worden, via de functie print_grid uit printer.h,
 * en moet er geprint worden hoeveel vlaggen nog beschikbaar zijn voor de speler.
 */

void run_game(int height, int width, int nr_of_mines){ // Loop om ervoor te zorgen dat het spel stopt
    nr_of_remaining_flags = nr_of_mines;
    while (continue_game){
        if (nr_of_unveiled + nr_of_mines < width * height && correct_flags != nr_of_mines){
            read_input(height, width, nr_of_mines);
        }
        else {
            print_grid_uncovered(height, width, nr_of_mines);
            save_to_file(1);
            printf("You won, good game \n");
            restart_game(height, width, nr_of_mines);
        }
    }
}

/*
 * Leest de input van de gebruiker via de console in, en roept daarna de juiste functie op.
 * De gebruiker kan 3 soorten inputs geven:
 *
 * 1)
 * F x y
 * Wordt gebruikt om een vlag te plaatsen op positie (x,y) in het veld. X en y zijn hier integers.
 * handle_input roept hierna de functie flag_coordinate op.
 *
 * 2)
 * R x y
 * Wordt gebruikt om het vakje met positie (x,y) te onthullen. X en y zijn hier integers.
 * handle_input roept hierna de functie reveal_coordinate op.
 *
 * 3)
 * P
 * Print het hele, onthulde veld.
 * handle_input roept hierna de functie print_grid_uncovered op.
 *
 *
 * Elk van deze functies (flag_coordinate, reveal_coordinate en print_grid_uncovered) moeten
 * nog door jullie geïmplementeerd worden.
 *
 */
void handle_input(int height, int width, int nr_of_mines) {
    char command;
    int x, y;
    char input_buffer[20];
    /* Lees het commando van de gebruiker in */
    fgets(input_buffer, 20, stdin);
    sscanf(input_buffer, "%c %i %i", &command, &x, &y);
    switch (command) {
        case 'F': // Flag
            flag_coordinate(x, y, height, width, nr_of_mines);
            break;
        case 'R': // Reveal
            reveal_coordinate(x, y, height, width, nr_of_mines);
            break;
        case 'P': // Print
            print_grid_uncovered(height, width, nr_of_mines);
            break;
        default:
            /* De speler gaf een commando in dat niet begrepen werd door deze functie: probeer opnieuw. */
            printf("Command '%c' not understood, please try again.\n", command);
            handle_input(height, width, nr_of_mines);
            
    }
}
