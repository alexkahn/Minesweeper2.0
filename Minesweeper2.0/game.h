#ifndef GAME_H_
#define GAME_H_

#include <stdio.h>

#include "grid.h"
#include "printer.h"
#include "inputOutput.h"
#include "GUI.h"
/*
 * Deze functie wordt in het begin, eens het veld geïnitialiseerd is, aangeroepen.
 * Eens deze functie is aangeroepen moet het spel in beurten verdergaan tot de speler gewonnen of verloren heeft.
 * Na elke beurt moet het hele veld automatisch geprint worden, via de functie print_grid uit printer.h,
 * en moet er geprint worden hoeveel vlaggen nog beschikbaar zijn voor de speler.
 */
void run_game(int height, int width, int nr_of_mines);
void handle_input(int height, int width, int nr_of_mines);
void reveal_coordinate(int x, int y, int height,int width,int nr_of_mines);
void flag_coordinate(int x, int y, int height, int width, int nr_of_mines);
void print_grid_uncovered(int height, int width, int nr_of_mines);


#endif /* GAME_H_ */
