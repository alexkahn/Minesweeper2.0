//
//  inputOutput.c
//  Minesweeper
//
//  Created by Alexandre Kahn on 02/12/2016.
//  Copyright © 2016 Alexandre Kahn. All rights reserved.
//  based on https://www.tutorialspoint.com/cprogramming/c_file_io.htm

#include "inputOutput.h"
#include <stdio.h>
#include <stdlib.h>
// The file will always have the same input
// Played games: 0000
// Wins: 0000

FILE *fp;
char buff[255];
int played_games = 0;
int wins = 0;

void open_file(){
    if ((fp = fopen ("games.txt","r"))){
        fp=fopen("games.txt", "r+"); //Als de file bestaat gewoon inlezen
        fscanf(fp, "%s", buff);
        played_games = atoi(buff);
        fgets(buff, 255, (FILE*)fp);
        wins =atoi(buff);
        printf("%i ", played_games);
        printf("%i", wins);
        fclose(fp);
    }
        else{                       //Als de file niet bestaat
        fp = fopen ("games.txt", "wb");
        fprintf(fp, "0 0");
        fclose(fp);
    }
}

void close_file(){
    fclose(fp);
}
void save_to_file(int won){
    fp=fopen("games.txt", "w+");
    played_games++;
    fprintf(fp, "%i", played_games);
    fputs(" ", fp);
    wins = won + wins;
    fprintf(fp, "%i", wins);
    close_file();
}
