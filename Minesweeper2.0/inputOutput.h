//
//  inputOutput.h
//  Minesweeper
//
//  Created by Alexandre Kahn on 02/12/2016.
//  Copyright © 2016 Alexandre Kahn. All rights reserved.
//

#ifndef inputOutput_h
#define inputOutput_h

#include <stdio.h>
void open_file();
//int played_games;
//int wins;
void close_file();
void save_to_file(int won);

#endif /* inputOutput_h */
