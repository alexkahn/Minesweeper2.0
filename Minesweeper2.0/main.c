//
//  main.c
//  Minesweeper2.0
//
//  Created by Alexandre Kahn on 04/12/2016.
//  Copyright © 2016 Alexandre Kahn. All rights reserved.
//

#include <stdlib.h>
#include <time.h>

#include "game.h"
#include "grid.h"
#include "inputOutput.h"
#include "GUI.h"

/*
 * Initialiseert eerst het veld, via de functie initialize_grid uit grid.c, en roept daarna de functie
 * run_game uit game.c aan om het spel effectief te starten.
 */

int main(int argc, char *argv[]) {
    /*
     * Naar deze statement hoef je nu niet te kijken.
     */
    srand(time(NULL));
    
    if (argc == 4){ //3 argumenten + ./main
        int height = atoi(argv[1]);
        int width = atoi(argv[2]);
        int nr_of_mines = atoi(argv[3]);
        open_file();
        
        
        initialize_grid(width, height, nr_of_mines);
        initialize_gui(width, height, nr_of_mines);
        
        /*
         * Start het eigenlijke spel.
         */
        run_game(height, width, nr_of_mines);
        
    }
    else{
        printf("You need three arguments \n");
    }
    return 0;
}

