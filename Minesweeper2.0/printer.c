
#include "printer.h"

/*
 * In dit bestand kan je de functie print_grid uit printer.h implementeren.
 */


void print_grid(int remaining_flags, int height, int width){ // Print de grid
    printf("Aantal vlaggen %i \n", remaining_flags);
    printf("     0  ");
    for (int t = 1; t < width; t++){
        printf("%i  ", t);
    }
    printf("\n");
    for (int y = 0; y < height; y++){
        printf("%i   ", y);
        for (int x = 0; x < width; x++){
            if ((get_cell(x, y)->state) == COVERED) {
                printf("| |");
            }
            else if ((get_cell(x, y)->state) == FLAGGED){
                printf("|🚩|");
            }
            else if ((get_cell(x, y)->state) == UNCOVERED){
                if (get_cell(x, y)->is_mine == 0) {
                    printf("|%i|", get_cell(x, y)->neighbouring_mines);
                }
                else{
                    printf("|💣|" );
                }
            }
        }
        printf("\n");
    }
}

void uncover(int remaining_flags, int height, int width){ // print het speelveld volledig, zonder de staat ervan aan te passen
    printf("Aantal vlaggen %i \n", remaining_flags);
    printf("     0  ");
    for (int t = 1; t < width; t++){
        printf("%i  ", t);
    }
    printf("\n");
    for (int y = 0; y < height; y++){
        printf("%i   ", y);
        for (int x = 0; x < width; x++){
            if ((get_cell(x, y)->state) == FLAGGED){
                printf("|🚩|");
            }
            else {
                if (get_cell(x, y)->is_mine == 0) {
                    printf("|%i|", get_cell(x, y)->neighbouring_mines);
                }
                else{
                    printf("|💣|" );
                }
            }
        }
        printf("\n");
    }
}
