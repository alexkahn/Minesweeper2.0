#ifndef PRINTER_H_
#define PRINTER_H_

#include <stdio.h>

#include "grid.h"
#include "settings.h"

// Mogelijkheden om in kleur te printen, komt van: http://stackoverflow.com/questions/3585846/color-text-in-terminal-applications-in-unix
#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m"

/*
 * Print het hele veld. Deze functie gaat ervan uit dat de dimensies (de breedte en de hoogte) van het veld
 * gelijk zijn aan de dimensies die gedefinieerd staan in settings.h.
 *
 * Hoe dit veld juist moet geprint worden, mag je zelf kiezen.
 */
void print_grid(int remaining_flags, int height, int width);
void uncover(int remaining_flags, int height, int width);

#endif /* PRINTER_H_ */
